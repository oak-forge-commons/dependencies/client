package pl.oakfusion.client;

public interface Writer {
    void write(String string);

    void writeLine(String string);
}
