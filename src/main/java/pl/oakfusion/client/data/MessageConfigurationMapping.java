package pl.oakfusion.client.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.*;
import pl.oakfusion.client.MessageConfiguration;
import pl.oakfusion.client.factory.*;

import java.io.*;
import java.util.*;

public class MessageConfigurationMapping {

    private final String ALPHABETIC_CHARACTERS_REGEX = "[A-Za-z]*";
    private final OptionsTreeBuilder treeBuilder;
    private final InputStream inputStream;
    private final ObjectMapper om;
    private final AbbreviationFactory abbreviationFactory;
    @Getter
    private final Map<String, String> commandsMap;
    private final Map<String, Map<String, String>> commandsOptions;
    @Getter
    @Setter
    private List<MessageConfiguration> commands;
    @Getter
    @Setter
    private Map<String, Tree<String>> optionTreesMap;

    public MessageConfigurationMapping() {
        commands = new ArrayList<>();
        inputStream = getClass().getClassLoader().getResourceAsStream("commands.yml");
        om = new ObjectMapper(new YAMLFactory());
        om.addMixIn(MessageConfigurationMapping.class, MessageConfigurationMappingMixIn.class);
        om.addMixIn(MessageConfiguration.class, MessageConfigurationMixIn.class);
        commandsMap = new HashMap<>();
        abbreviationFactory = new AbbreviationFactory();
        commandsOptions = new HashMap<>();
        optionTreesMap = new HashMap<>();
        treeBuilder = new OptionsTreeBuilder();
    }

    public MessageConfigurationMapping(String file, ObjectMapper objectMapper, AbbreviationFactory abbreviationFactory, OptionsTreeBuilder optionsTreeBuilder) {
        commands = new ArrayList<>();
        inputStream = getClass().getClassLoader().getResourceAsStream(file);
        om = objectMapper;
        commandsMap = new HashMap<>();
        this.abbreviationFactory = abbreviationFactory;
        commandsOptions = new HashMap<>();
        optionTreesMap = new HashMap<>();
        treeBuilder = optionsTreeBuilder;
    }

    public void parseCommands() throws IOException {
        commands = om.readValue(inputStream, MessageConfigurationMapping.class).getCommands();
        List<String> abbreviations;
        List<String> cmdOptAbbreviations;
        Map<String, String> commandOptionsMap;
        Tree<String> commandTree;
        Map<String, String> optionsShortestFormsMap;
        List<String> commandOptionsWithShortestForms;
        List<List<String>> commandOptions;
        List<String> singlePathOptions;

        for (MessageConfiguration command : commands) {
            optionsShortestFormsMap = new HashMap<>();
            commandOptions = new ArrayList<>();

            if (command.getOptions() != null) {
                for (String path : command.getOptions()) {
                    singlePathOptions = new ArrayList<>();
                    commandOptionsWithShortestForms = Arrays.asList(path.split("\\s"));

                    for (int i = 0; i < commandOptionsWithShortestForms.size(); i++) {
                        if (commandOptionsWithShortestForms.get(i).matches(ALPHABETIC_CHARACTERS_REGEX)) {
                            optionsShortestFormsMap.put(commandOptionsWithShortestForms.get(i), commandOptionsWithShortestForms.get(i + 1));
                            singlePathOptions.add(commandOptionsWithShortestForms.get(i));
                            i++;
                        } else {
                            singlePathOptions.add(commandOptionsWithShortestForms.get(i));
                        }
                    }
                    commandOptions.add(singlePathOptions);
                }
            }
            /*
             *	Build valid tree from YAML command configuration with option mnemonic as a root
             */
            optionTreesMap.put(command.getMnemonic(), treeBuilder.buildTree(command.getMnemonic(), commandOptions));

            /*
             *  Create all abbreviations for each Command Mnemonic
             */
            abbreviations = abbreviationFactory.getAbbreviations(command.getMnemonic(), command.getShortestForm());
            for (String abbreviation : abbreviations) {
                /*
                 * Put each abbreviation into a map parsing one into a full mnemonic of the command
                 */
                commandsMap.put(abbreviation, command.getMnemonic());
                commandOptionsMap = new HashMap<>();
                /*
                 * Get options tree for the current mnemonic and then traverse it's children inorder to create
                 * abbreviations for each option possible
                 */
                commandTree = optionTreesMap.get(command.getMnemonic());
                if (commandTree != null) {
                    for (String commandOption : getAllTextOptions(commandTree)) {
                        /*
                         * Create abbreviations for each command option
                         */
                        cmdOptAbbreviations = abbreviationFactory.getAbbreviations(commandOption, optionsShortestFormsMap.get(commandOption));
                        for (String singleOptionAbbreviation : cmdOptAbbreviations) {
                            commandOptionsMap.put(singleOptionAbbreviation, commandOption);
                        }
                    }
                }
                commandsOptions.put(abbreviation, commandOptionsMap);
            }
        }
    }

    private List<String> getAllTextOptions(Tree<String> commandTree) {
        List<String> optionsList = new ArrayList<>();
        populateOptionsList(commandTree.getRoot(), optionsList);
        return optionsList;
    }

    private void populateOptionsList(Tree.Node<String> node, List<String> optionsList) {
        for (Tree.Node<String> child : node.getChildren()) {
            if (!optionsList.contains(child.getData()) && child.getData().matches(ALPHABETIC_CHARACTERS_REGEX)) {
                optionsList.add(child.getData());
            }
            populateOptionsList(child, optionsList);
        }
    }

    private boolean checkIfCommandExists(String command) {
        return commandsMap.containsKey(command);
    }

    public String getCommand(String command) {
        return commandsMap.get(command);
    }

    public Map<String, Map<String, String>> getCommandsOptions() {
        return this.commandsOptions;
    }

}
