package pl.oakfusion.client.data;

import lombok.*;

@Getter
@Setter
public class UserInputParsingResult {

    private UserInput userInput = new UserInput();
    private String hints = null;

    public UserInputParsingResult() {
    }
}
