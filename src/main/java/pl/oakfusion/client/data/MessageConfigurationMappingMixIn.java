package pl.oakfusion.client.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.oakfusion.client.factory.*;

import java.io.File;
import java.util.Map;

public abstract class MessageConfigurationMappingMixIn {
    @JsonIgnore
    OptionsTreeBuilder treeBuilder;
    @JsonIgnore
    File file;
    @JsonIgnore
    ObjectMapper om;
    @JsonIgnore
    Map<String, String> commandsMap;
    @JsonIgnore
    Map<String, Map<String, String>> commandsOptions;
    @JsonIgnore
    AbbreviationFactory abbreviationFactory;
    @JsonIgnore
    Map<String, Tree<String>> optionTreesMap;
}
