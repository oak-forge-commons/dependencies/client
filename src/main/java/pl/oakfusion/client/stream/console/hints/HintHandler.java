package pl.oakfusion.client.stream.console.hints;

import pl.oakfusion.client.data.UserInput;
import pl.oakfusion.data.message.Event;

@FunctionalInterface
public interface HintHandler {

    String handle(UserInput userInput);
}
