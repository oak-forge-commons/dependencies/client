package pl.oakfusion.client.view;

public interface ViewRendererMapping {

    void registerViewHandlers();
}
