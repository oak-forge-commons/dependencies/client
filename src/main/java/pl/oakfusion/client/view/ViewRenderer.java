package pl.oakfusion.client.view;

import pl.oakfusion.client.data.DTO;
import pl.oakfusion.data.message.Event;

public interface ViewRenderer<T extends Event> {

    void render(T event);

    default void render(DTO dto) {}
}