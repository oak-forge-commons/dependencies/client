package pl.oakfusion.client;


import lombok.*;
import pl.oakfusion.data.message.Command;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MessageConfiguration {

    private String name;
    private String mnemonic;
    private String shortestForm;
    private List<String> options;

    @Override
    public String toString() {
        return "CommandConfiguration{\n" +
                "name='" + name + '\'' + '\n' +
                "mnemonic='" + mnemonic + '\'' + '\n' +
                "shortestForm='" + shortestForm + '\'' + '\n' +
                "options=" + options + '\n' +
                '}';
    }
}
