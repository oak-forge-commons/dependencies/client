package pl.oakfusion.client.parser;

import pl.oakfusion.client.data.*;

import java.util.*;

class ShortRangeScanHandlers {

	private final Map<String, CommandParserHandler> commandHandlers;

	ShortRangeScanHandlers(Map<String, CommandParserHandler> commandHandlers) {
		this.commandHandlers = commandHandlers;
	}

	public void handle(Iterator<String> iterator, CommandParserHandlerContext context) {
		if(iterator.hasNext()) {
			String next = iterator.next();
			Tree.Node<String> nextNode = context.getCurrentNode().getChild(next);
			if (nextNode == null) {
				throw new UserInputException();
			}
			context.getUserInput().getCommandOptions().add(nextNode.getData());
			commandHandlers.get(context.getCurrentNode().getData() + "_" + nextNode.getData()).handle(iterator, new CommandParserHandlerContext(nextNode, context.getUserInput()));
		}
	}

	public void handleOptional(Iterator<String> iterator, CommandParserHandlerContext context) {
		if (iterator.hasNext()) {
			String next = iterator.next();
			Tree.Node<String> nextNode = context.getCurrentNode().getChild(next);
			if (nextNode == null) {
				throw new UserInputException();
			}
			context.getUserInput().getCommandOptions().add(nextNode.getData());
			if (iterator.hasNext()) {
				throw new UserInputException();
			}
		}
	}
}
