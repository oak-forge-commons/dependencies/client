package pl.oakfusion.client.parser;

import pl.oakfusion.client.data.*;

import java.util.*;

import static java.lang.String.valueOf;

class MoveCommandHandlers {

    private final String FLOAT_REGEX = "[-]?[0-9]*(\\.[0-9])*";
    private final String INT_REGEX = "[-]?[0-9]*";
    private final Map<String, CommandParserHandler> commandHandlers;

    MoveCommandHandlers(Map<String, CommandParserHandler> commandHandlers) {
        this.commandHandlers = commandHandlers;
    }

    public void handle(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        Tree.Node<String> nextNode = context.getCurrentNode().getChild(iterator.next());
        if (nextNode == null) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().add(nextNode.getData());
        commandHandlers.get(context.getCurrentNode().getData() + "_" + nextNode.getData()).handle(iterator, new CommandParserHandlerContext(nextNode, context.getUserInput()));
    }

    public void handleManual(Iterator<String> iterator, CommandParserHandlerContext context) {
        handleMoveMode(iterator, context, FLOAT_REGEX);
    }

    public void handleAutomatic(Iterator<String> iterator, CommandParserHandlerContext context) {
        handleMoveMode(iterator, context, INT_REGEX);
    }

    private void handleMoveMode(Iterator<String> iterator, CommandParserHandlerContext context, String regex) {
        List<String> numericOptions = new ArrayList<>();
        String next = "";
        boolean isPreviousOptionNumeric = true;
        while (iterator.hasNext() && isPreviousOptionNumeric) {
            next = iterator.next();
            if (next.matches(regex)) {
                numericOptions.add(next);
            } else {
                isPreviousOptionNumeric = false;
            }

        }
        Tree.Node<String> nextNode = context.getCurrentNode().getChild(valueOf(numericOptions.size()));
        if (nextNode == null) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().addAll(numericOptions);

        nextNode = nextNode.getChild(next);
        if (nextNode == null) {
            throw new UserInputException();
        } else {
            context.getUserInput().getCommandOptions().add(next);
        }
        if(iterator.hasNext()) {
            throw new UserInputException();
        }
    }
}
