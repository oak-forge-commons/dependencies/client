package pl.oakfusion.client.parser;

import pl.oakfusion.client.data.*;
import pl.oakfusion.client.stream.StreamWriter;
import pl.oakfusion.client.stream.console.hints.*;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.valueOf;

public class UserInputParser {

    private final String FLOAT_REGEX = "[-]?[0-9]*(\\.[0-9])*";
    private final String INT_REGEX = "[-]?[0-9]*";
    private final String ALPHANUMERIC_REGEX = "[A-Za-z0-9]*";
    private final Map<String, CommandParserHandler> commandHandlers;
    private final MessageConfigurationMapping messageConfigurationMapping;
    private final Map<String, String> commands;
    private final Map<String, Map<String, String>> commandsOptionsMap;
    private final HintHandler hintHandler;
    private final PhasersHandlers phasersHandlers;
    private final TorpedoHandlers torpedoHandlers;
    private final ShortRangeScanHandlers shortRangeScanHandlers;
    private final MoveCommandHandlers moveCommandHandlers;

    public UserInputParser() throws IOException {
        messageConfigurationMapping = new MessageConfigurationMapping();
        messageConfigurationMapping.parseCommands();
        commands = messageConfigurationMapping.getCommandsMap();
        commandsOptionsMap = messageConfigurationMapping.getCommandsOptions();
        hintHandler = new DefaultHintHandler(commands, messageConfigurationMapping.getOptionTreesMap());
        commandHandlers = new HashMap<>();
        phasersHandlers = new PhasersHandlers(commandHandlers);
        torpedoHandlers = new TorpedoHandlers(commandHandlers);
        shortRangeScanHandlers = new ShortRangeScanHandlers(commandHandlers);
        moveCommandHandlers = new MoveCommandHandlers(commandHandlers);
        initializeCommandHandlers();
    }

    public UserInputParser(MessageConfigurationMapping messageConfigurationMapping) throws IOException {
        this.messageConfigurationMapping = messageConfigurationMapping;
        this.messageConfigurationMapping.parseCommands();
        commands = this.messageConfigurationMapping.getCommandsMap();
        commandsOptionsMap = this.messageConfigurationMapping.getCommandsOptions();
        hintHandler = new DefaultHintHandler(commands, this.messageConfigurationMapping.getOptionTreesMap());
        commandHandlers = new HashMap<>();
        phasersHandlers = new PhasersHandlers(commandHandlers);
        torpedoHandlers = new TorpedoHandlers(commandHandlers);
        shortRangeScanHandlers = new ShortRangeScanHandlers(commandHandlers);
        moveCommandHandlers = new MoveCommandHandlers(commandHandlers);
        initializeCommandHandlers();
    }

    private void initializeCommandHandlers() {
        commandHandlers.put("SRSCAN", shortRangeScanHandlers::handle);
        commandHandlers.put("SRSCAN_NO", shortRangeScanHandlers::handleOptional);
        commandHandlers.put("SRSCAN_CHART", shortRangeScanHandlers::handleOptional);
        commandHandlers.put("STATUS", this::handleCommandWithoutOptions);
        commandHandlers.put("LRSCAN", this::handleCommandWithoutOptions);
        commandHandlers.put("CHART", this::handleCommandWithoutOptions);
        commandHandlers.put("DAMAGES", this::handleCommandWithoutOptions);
        commandHandlers.put("MOVE", moveCommandHandlers::handle);
        commandHandlers.put("MOVE_AUTOMATIC", moveCommandHandlers::handleAutomatic);
        commandHandlers.put("MOVE_MANUAL", moveCommandHandlers::handleManual);
        commandHandlers.put("WARP", this::handleSingleFloatOption);
        commandHandlers.put("IMPULSE", this::callNextHandler);
        commandHandlers.put("IMPULSE_AUTOMATIC", this::handleIntegerOptions);
        commandHandlers.put("IMPULSE_MANUAL", this::handleFloatOptions);
        commandHandlers.put("SHIELDS", this::callNextHandler);
        commandHandlers.put("SHIELDS_UP", this::handleCommandWithoutOptions);
        commandHandlers.put("SHIELDS_DOWN", this::handleCommandWithoutOptions);
        commandHandlers.put("SHIELDS_TRANSFER", this::handleSingleFloatOption);
        commandHandlers.put("PHASERS", phasersHandlers::handle);
        commandHandlers.put("PHASERS_MANUAL", phasersHandlers::handleManual);
        commandHandlers.put("PHASERS_AUTOMATIC", phasersHandlers::handleAutomatic);
        commandHandlers.put("PHASERS_NO", phasersHandlers::handleNo);
        commandHandlers.put("CAPTURE", this::handleCommandWithoutOptions);
        commandHandlers.put("REPORT", this::handleCommandWithoutOptions);
        commandHandlers.put("COMPUTER", this::handleCommandWithoutOptions);
        commandHandlers.put("CLOAK", this::handleTextOption);
        commandHandlers.put("TORPEDO", torpedoHandlers::handle);
        commandHandlers.put("TORPEDO_TARGETS", torpedoHandlers::handleTorpedoTargets);
        commandHandlers.put("DOCK", this::handleCommandWithoutOptions);
        commandHandlers.put("REST", this::handleSingleIntOption);
        commandHandlers.put("MAYDAY", this::handleCommandWithoutOptions);
        commandHandlers.put("ABANDON", this::handleCommandWithoutOptions);
        commandHandlers.put("DESTRUCT", this::handleCommandWithoutOptions);
        commandHandlers.put("QUIT", this::handleCommandWithoutOptions);
        commandHandlers.put("SENSORS", this::handleCommandWithoutOptions);
        commandHandlers.put("ORBIT", this::handleCommandWithoutOptions);
        commandHandlers.put("TRANSPORT", this::handleCommandWithoutOptions);
        commandHandlers.put("SHUTTLE", this::handleCommandWithoutOptions);
        commandHandlers.put("MINE", this::handleCommandWithoutOptions);
        commandHandlers.put("CRYSTALS", this::handleCommandWithoutOptions);
        commandHandlers.put("PLANETS", this::handleCommandWithoutOptions);
        commandHandlers.put("FREEZE", this::handleFreeze);
        commandHandlers.put("REQUEST", this::handleTextOption);
        commandHandlers.put("DEATHRAY", this::handleCommandWithoutOptions);
        commandHandlers.put("PROBE", this::callNextHandler);
        commandHandlers.put("PROBE_AUTOMATIC", this::handleIntegerOptions);
        commandHandlers.put("PROBE_MANUAL", this::handleFloatOptions);
        commandHandlers.put("PROBE_ARMED", this::handleProbeArmed);
        commandHandlers.put("EMEXIT", this::handleCommandWithoutOptions);
        commandHandlers.put("HELP", this::handleHelp);
        commandHandlers.put("SELECT", this::handleSelect);
    }

    private void handleCommandWithoutOptions(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (iterator.hasNext()) {
            throw new UserInputException();
        }
    }

    private void callNextHandler(Iterator<String> iterator, CommandParserHandlerContext context) {
        Tree.Node<String> nextNode = getNextOptionIfCorrect(iterator, context);
        context.getUserInput().getCommandOptions().add(nextNode.getData());
        commandHandlers.get(context.getCurrentNode().getData() + "_" + nextNode.getData()).handle(iterator, new CommandParserHandlerContext(nextNode, context.getUserInput()));
    }

    private Tree.Node<String> getNextOptionIfCorrect(Iterator<String> iterator, CommandParserHandlerContext context) throws UserInputException {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        Tree.Node<String> nextNode = context.getCurrentNode().getChild(iterator.next());
        if (nextNode == null) {
            throw new UserInputException();
        }
        return nextNode;
    }

    private void handleIntegerOptions(Iterator<String> iterator, CommandParserHandlerContext context) {
        handleNumericOptionsThatMatch(iterator, context, INT_REGEX);
    }

    private void handleNumericOptionsThatMatch(Iterator<String> iterator, CommandParserHandlerContext context, String int_regex) {
        List<String> numericOptions = getWhileMatches(iterator, int_regex);
        Tree.Node<String> nextNode = context.getCurrentNode().getChild(valueOf(numericOptions.size()));
        if (nextNode == null) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().addAll(numericOptions);
    }

    private void handleFloatOptions(Iterator<String> iterator, CommandParserHandlerContext context) {
        handleNumericOptionsThatMatch(iterator, context, FLOAT_REGEX);
    }

    private void handleSingleFloatOption(Iterator<String> iterator, CommandParserHandlerContext context) {
        handleSingleNumericOption(iterator, context, FLOAT_REGEX);
    }

    private void handleSingleNumericOption(Iterator<String> iterator, CommandParserHandlerContext context, String regex) {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        String next = iterator.next();
        if (!next.matches(regex) || iterator.hasNext()) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().add(next);
    }

    private void handleSingleIntOption(Iterator<String> iterator, CommandParserHandlerContext context) {
        handleSingleNumericOption(iterator, context, INT_REGEX);
    }

    private void handleFreeze(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        String next = iterator.next();
        if (!next.matches(".*\\.TRK") || iterator.hasNext()) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().add(next);
    }

    private void handleSelect(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        String next = iterator.next();
        if (!(next.matches(INT_REGEX) || next.matches(ALPHANUMERIC_REGEX)) || iterator.hasNext()) {
            throw new UserInputException();
        }

        context.getUserInput().getCommandOptions().add(next);
    }

    private void handleProbeArmed(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        String next = iterator.next();
        Tree.Node<String> nextNode = context.getCurrentNode().getChild(next);
        if (nextNode == null) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().add(nextNode.getData());
        commandHandlers.get(context.getCurrentNode().getParent().getData() + "_" + nextNode.getData()).handle(iterator, new CommandParserHandlerContext(nextNode, context.getUserInput()));
    }

    private void handleHelp(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        String next = iterator.next();
        if (!commands.containsKey(next) || iterator.hasNext()) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().add(commands.get(next));
    }

    private void handleTextOption(Iterator<String> iterator, CommandParserHandlerContext context) {
        Tree.Node<String> nextNode = getNextOptionIfCorrect(iterator, context);
        if (iterator.hasNext()) {
            throw new UserInputException();
        }
        context.getUserInput().getCommandOptions().add(nextNode.getData());
    }

    public UserInputParsingResult parseInput(List<String> input) {
        UserInputParsingResult userInputParsingResult = new UserInputParsingResult();
        try {
            String mnemonic = input.get(0).toUpperCase();
            String finalMnemonic;
            List<String> commandOptions = new ArrayList<>();
            if (input.size() > 1) {
                for (int i = 1; i < input.size(); i++) {
                    commandOptions.add(input.get(i).toUpperCase());
                }
            }

            finalMnemonic = commands.get(mnemonic);
            if (finalMnemonic == null) {
                userInputParsingResult.getUserInput().setMnemonic(mnemonic);
                throw new UserInputException();
            }
            userInputParsingResult.getUserInput().setMnemonic(finalMnemonic);

            commandOptions = commandOptions
                    .stream()
                    .map(option -> commandsOptionsMap
                            .get(finalMnemonic).computeIfAbsent(option, o -> option))
                    .collect(Collectors.toList());

            Tree<String> commandTree = messageConfigurationMapping.getOptionTreesMap().get(finalMnemonic);

            CommandParserHandlerContext context = new CommandParserHandlerContext(commandTree.getRoot(), userInputParsingResult.getUserInput());
            commandHandlers.get(finalMnemonic).handle(commandOptions.iterator(), context);

            userInputParsingResult.setUserInput(context.getUserInput());
        } catch (UserInputException e) {
            userInputParsingResult.setHints(hintHandler.handle(userInputParsingResult.getUserInput()));
        }

        return userInputParsingResult;
    }

    private List<String> getWhileMatches(Iterator<String> iterator, String regex) throws UserInputException {
        List<String> options = new ArrayList<>();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.matches(regex)) {
                options.add(next);
            } else {
                throw new UserInputException();
            }
        }
        return options;
    }
}
