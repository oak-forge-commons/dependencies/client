package pl.oakfusion.client.parser;

import pl.oakfusion.client.data.*;

import java.util.*;

import static java.lang.String.valueOf;

class PhasersHandlers {

    private final String FLOAT_REGEX = "[-]?[0-9]*(\\.[0-9])*";
    private final String INT_REGEX = "[-]?[0-9]*";
    private final String MNEMONIC = "PHASERS";
    private final String AUTOMATIC_HANDLER = "AUTOMATIC";
    private final Map<String, CommandParserHandler> commandHandlers;

    PhasersHandlers(Map<String, CommandParserHandler> commandHandlers) {
        this.commandHandlers = commandHandlers;
    }

	void handle(Iterator<String> iterator, CommandParserHandlerContext context) {
		if (!iterator.hasNext()) {
			throw new UserInputException();
		}
		String next = iterator.next();

        if (next.matches(FLOAT_REGEX)) {
			context.getUserInput().getCommandOptions().add(AUTOMATIC_HANDLER);
			if (next.matches(INT_REGEX)) {
				commandHandlers.get(MNEMONIC + "_" + AUTOMATIC_HANDLER).handle(iterator, new CommandParserHandlerContext(context.getCurrentNode().getChild("AUTOMATIC"), context.getUserInput(), Collections.singletonList(next)));
			} else {
				throw new UserInputException();
			}
		} else {
			Tree.Node<String> nextNode = context.getCurrentNode().getChild(next);
			if (nextNode == null || context.getUserInput().getCommandOptions().contains(nextNode.getData())) {
				throw new UserInputException();
			}
			context.getUserInput().getCommandOptions().add(nextNode.getData());
			commandHandlers.get(context.getCurrentNode().getData() + "_" + nextNode.getData()).handle(iterator, new CommandParserHandlerContext(nextNode, context.getUserInput()));
		}
	}

	void handleManual(Iterator<String> iterator, CommandParserHandlerContext context) {
		List<String> numericOptions = new ArrayList<>();
		String optionalParameter = null;

        if (!iterator.hasNext()) {
			throw new UserInputException();
		}

        while (iterator.hasNext()) {
			String next = iterator.next();
			if (next.matches(INT_REGEX)) {
				numericOptions.add(next);
			} else if (!numericOptions.isEmpty()) {
                Tree.Node<String> nextNode = context.getCurrentNode().getChildren().stream().findAny().orElse(null);
                if (nextNode == null) {
                    throw new UserInputException();
                }
                handleOptional(context, next, nextNode);
                optionalParameter = next;
            } else {
                throw new UserInputException();
            }
        }

        context.getUserInput().getCommandOptions().addAll(numericOptions);
        if (optionalParameter != null) {
            context.getUserInput().getCommandOptions().add(optionalParameter);
        }
    }

    private void handleOptional(CommandParserHandlerContext context, String next, Tree.Node<String> nextNode) {
        nextNode = nextNode.getChild(next);
        if (nextNode == null) {
            throw new UserInputException();
        }
        if (context.getUserInput().getCommandOptions().contains(next)) {
            throw new UserInputException();
        }
    }

    void handleAutomatic(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (context.getOptionParameters().isEmpty()) {
            if (!iterator.hasNext()) {
                throw new UserInputException();
            }
            String next = iterator.next();
            if (!next.matches(INT_REGEX)) {
                throw new UserInputException();
            }
            context.getOptionParameters().add(next);
        }

        if (iterator.hasNext()) {
            String next = iterator.next();
            Tree.Node<String> nextNode = context.getCurrentNode().getChild(valueOf(context.getOptionParameters().size()));
            if (nextNode == null) {
                throw new UserInputException();
            }
            handleOptional(context, next, nextNode);
            context.getUserInput().getCommandOptions().addAll(context.getOptionParameters());
            context.getUserInput().getCommandOptions().add(next);
        } else {
			context.getUserInput().getCommandOptions().addAll(context.getOptionParameters());
		}

	}

	void handleNo(Iterator<String> iterator, CommandParserHandlerContext context) {
		if (!iterator.hasNext()) {
			throw new UserInputException();
		}
		commandHandlers.get(MNEMONIC).handle(iterator, new CommandParserHandlerContext(context.getCurrentNode().getParent(), context.getUserInput()));
	}
}

