package pl.oakfusion.client.factory;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class AbbreviationFactory {

    public List<String> getAbbreviations(String name, String shortestAbbreviation) {
        if (shortestAbbreviation == null || !name.toUpperCase().startsWith(shortestAbbreviation.toUpperCase())) {
            return emptyList();
        }

        if (shortestAbbreviation.equals("")) {
            return singletonList(name.toUpperCase());
        }

        List<String> abbreviations = new ArrayList<>(singletonList(shortestAbbreviation.toUpperCase()));

        name.chars()
                .skip(shortestAbbreviation.length())
                .mapToObj(i -> (char) i)
                .forEach(character -> abbreviations.add((abbreviations.get(abbreviations.size() - 1) + character).toUpperCase()));

        return abbreviations;
    }

    public List<String> getAbbreviations(String name) {
        return getAbbreviations(name.toUpperCase(), String.valueOf(name.charAt(0)));
    }
}
