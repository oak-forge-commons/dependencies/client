package pl.oakfusion.client.stream.console.hints

import pl.oakfusion.client.data.MessageConfigurationMapping
import pl.oakfusion.client.data.UserInput
import spock.lang.Specification

class DefaultHintHandlerSpockTest extends Specification {

    private MessageConfigurationMapping messageConfigurationMapping;
    private UserInput userInput
    private HintHandler hintHandler

    def setup() {
        messageConfigurationMapping = new MessageConfigurationMapping()
        userInput = new UserInput()
        messageConfigurationMapping.parseCommands()
        hintHandler = new DefaultHintHandler(messageConfigurationMapping.getCommandsMap(),
                messageConfigurationMapping.getOptionTreesMap())
    }

    def "should show correct hints for commands"() {
        when:
        userInput.setMnemonic(mnemonic)
        userInput.setCommandOptions(options)

        then:
        for (String path : expectedPaths) {
            hintHandler.handle(userInput).contains(path)
        }

        where:
        mnemonic  | options                         | expectedPaths
        "WARP"    | ["RTA", "2"]                    | ["WARP <number1>"]
        "MOVE"    | ["A", "2"]                      | ["MOVE AUTOMATIC <number1> <number2>", "MOVE AUTOMATIC <number1> <number2> <number3> <number4>"]
        "IMPULSE" | ["M", "3", "4", "5"]            | ["IMPULSE MANUAL <number1>", "IMPULSE MANUAL <number1> <number2>"]
        "PROBE"   | ["AR", "M", "1", "3", "7", "5"] | ["PROBE ARMED MANUAL <number1>", "IMPULSE ARMED MANUAL <number1> <number2>"]
        "PHASERS" | ["M", "ABS"]                    | ["PHASERS MANUAL <number1> <number2> <number3> ... <numberN>", "PHASERS MANUAL <number1> <number2> <number3> ... <numberN> NO"]
        "SHIELDS" | ["ABC"]                         | ["SHIELDS UP", "SHIELDS DOWN", "SHIELDS TRANSFER <number1>"]
    }
}
